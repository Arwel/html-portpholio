/// <reference path="global.d.ts" />

import 'babel-polyfill';
import 'unfetch/polyfill';

import navManager from './components/nav/Nav';
import carouselManager from './components/carousel';
import inputManager from './components/input';
import mobileMenuManager from './components/nav/MobileMenu';
import mouseWheelSupport from './components/nav/MouseWheel';
import contactForm from './components/email';

document.addEventListener('DOMContentLoaded', () => {
    navManager();
    carouselManager();
    inputManager();
    mobileMenuManager();
    mouseWheelSupport();
    contactForm();
});
