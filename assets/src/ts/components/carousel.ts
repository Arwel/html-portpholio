import 'owl.carousel';
import { debounce } from '../helpers';

declare const $: any;

const getItemsCount = () =>  {
    const width = document.body.clientWidth;

    if (width < 991) {
        return 2;
    } else if (width < 1920) {
        return 7;
    } else {
        return 7;
    }
};

export default (context = document.body) => {
    for (const carousel of Array.from(context.querySelectorAll('[data-component="carousel"]'))) {
        let isStartItem = true;
        let isEndItem = false;
        const $carousel = $(carousel);
        const itemsCount = getItemsCount();

        $carousel.owlCarousel({
            // loop: true,
            nav: true,
            dots: false,
            items: itemsCount
        });

        $carousel.on('changed.owl.carousel', debounce(({item}) => {
            isStartItem = item.index === 0;
            isEndItem = item.index + itemsCount >= item.count;
        }, 150));

        const stage = carousel.querySelector('.owl-stage');
        stage && stage.addEventListener('wheel', (e: any) => {
            if (e.deltaY > 0) {
                if (!isEndItem) {
                    e.preventDefault();
                    e.stopPropagation();
                }

                $carousel.trigger('next.owl');
            } else {
                if (!isStartItem) {
                    e.preventDefault();
                    e.stopPropagation();
                }

                $carousel.trigger('prev.owl');
            }
        });
    }
};