import Target from './Target';


class Nav {
    protected targets;
    protected active;
    protected wrapper;

    constructor (wrapper) {
        this.targets = new Map();
        this.wrapper = document.querySelector('[data-role="page-wrapper"]');

        for (const link of wrapper.querySelectorAll('[data-role="link"]')) {
            const hash = this.getLinkHash((link as any).href);
            link.addEventListener('click', this.linkClickHandler(hash));

            if (link.classList.contains('active')) {
                this.active = link;
            }

            if (!this.targets.has(hash)) {
                this.targets.set(hash, new Target(hash));
            }
        }

        this.wrapper.addEventListener('navigate', this.navigate);

        window.addEventListener('resize', this.windowResizeHandler);
        this.windowResizeHandler();
    }

    windowResizeHandler = () => {
        this.targets.forEach(target => {
            target.updateRect();
        });
    };

    navigate = ({detail}) => {
        console.log('nav', detail);
        const target = this.targets.get(detail);
        if (!target) {
            return;
        }

        const {top} = target.getRect();

        this.wrapper.scroll({
            top,
            left: 0,
            behavior: 'smooth'
        });
    };

    linkClickHandler = hash => (event) => {
        event.preventDefault();
        event.stopPropagation();

        this.navigate({detail: hash});
        document.body.classList.remove('mobile-menu-open');
    };

    getLinkHash(link) {
        return link.substring(link.indexOf('#'));
    }
}


export default (context = document.body) => {
    for (const nav of Array.from(context.querySelectorAll('[data-component="nav"]'))) {
        new Nav(nav)
    }
};