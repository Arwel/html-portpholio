import { debounce, trigger } from '../../helpers';

class MouseWheelNav {
    protected activePage;
    protected wrapper;

    constructor (wrapper) {
        this.wrapper = wrapper;

        const intersectionObserver = new IntersectionObserver(this.pageIntersectHandler, {
            root: wrapper,
            threshold: .25
        });

        for (const page of Array.from<HTMLElement>(wrapper.querySelectorAll('[data-role="page"]'))) {
            intersectionObserver.observe(page);
        }

        document.addEventListener('wheel', debounce(this.documentWheelHandler, 75));
    }

    pageIntersectHandler = entities => {
        for (const {isIntersecting, target} of entities) {
            if (isIntersecting) {
                this.activePage = target;
            }
        }
    };

    documentWheelHandler = ({deltaY}) => {
        let hash = null;
        const {nextElementSibling, previousElementSibling} = this.activePage;

        if (deltaY > 0 && nextElementSibling) {
            hash = nextElementSibling.id;
        } else if (previousElementSibling) {
            hash = previousElementSibling.id;
        }

        if (hash) {
            console.log(hash);
            trigger(this.wrapper, 'navigate', {detail: `#${hash}`});
        }
    };
}

export default () => {
    new MouseWheelNav(document.querySelector('[data-role="page-wrapper"]'))
};