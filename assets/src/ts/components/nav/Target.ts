export default class Target {
    protected target;
    protected rect;

    constructor (selector) {
        this.target = document.querySelector(selector);
        this.updateRect();
    }

    updateRect = () => {
        requestAnimationFrame(() => {
            this.rect = {top: this.target.offsetTop}
        });
    };

    getRect = () => {
        return this.rect;
    };
}