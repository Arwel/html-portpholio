const prevent = event => {
    event.preventDefault();
    event.stopPropagation();
};

const openMobileMenu = event => {
    prevent(event);

    document.body.classList.add('mobile-menu-open');
};

const closeMobileMenu = event => {
    prevent(event);

    document.body.classList.remove('mobile-menu-open');
};

export default () => {
    const body = document.body;

    const openBtn = body.querySelector('[data-role="mobile-menu-open-btn"]');
    openBtn && openBtn.addEventListener('click', openMobileMenu);

    const closeBtn = body.querySelector('[data-role="mobile-menu-close-btn"]');
    closeBtn && closeBtn.addEventListener('click', closeMobileMenu);
};