const inputFocusHandler = ({classList}) => () => {
    classList.add('focus');
};

const inputBlurHandler = ({classList}) => () => {
    classList.remove('focus');
};

const inputInputHandler = ({classList}) => ({target}) => {
    if (target.value.length < 1) {
        classList.remove('has-value');
    } else {
        classList.add('has-value');
    }
};

export default (context = document.body) => {
    for (const inputWrapper of Array.from(context.querySelectorAll('[data-component="input"]'))) {
        const input = inputWrapper.querySelector('[data-role="input"]');
        if (!input) {
            continue;
        }

        input.addEventListener('focus', inputFocusHandler(inputWrapper));
        input.addEventListener('blur', inputBlurHandler(inputWrapper));
        input.addEventListener('input', inputInputHandler(inputWrapper));
    }
};