import * as emailjs from 'emailjs-com';


class Form {
    protected formProps;
    protected isLoading;
    protected form;

    constructor (form: HTMLFormElement) {
        this.formProps = {};
        this.isLoading = false;
        this.form = form;

        console.log(form);

        for (const input of Array.from<HTMLInputElement>(form.querySelectorAll('[data-role="input"]'))) {
            input.addEventListener('input', this.inputInputHandler);
        }

        form.addEventListener('submit', this.formSubmitHandler);

        const cancelBtn = form.querySelector('[data-role="cancel-btn"]');
        cancelBtn && cancelBtn.addEventListener('click', this.cancelClickHandler);
    }

    inputInputHandler = ({target}) => {
        if (this.isLoading) {
            return;
        }

        this.formProps[target.name] = target.value;
    };

    cancelClickHandler = () => {
        if (this.isLoading) {
            return;
        }

        this.formProps = {};
    };

    formSubmitHandler = async e => {
        e.preventDefault();
        e.stopPropagation();

        if (this.isLoading) {
            return;
        }

        this.isLoading = true;
        this.form.classList.add('loading');

        try {
            await emailjs.send('gmail','template_J9mbYiaU', this.formProps, 'user_s6gV7ZjP4Xqdr2cPS2JlX');

            alert('Sended!');
        } catch (e) {
            console.log('Error', e);

            alert(e);
        } finally {
            this.isLoading = false;
            this.form.classList.remove('loading');
        }
    };
}

export default () => {
    for (const form of Array.from<HTMLFormElement>(document.querySelectorAll('[data-component="email-form"]'))) {
        new Form(form);
    }
};
