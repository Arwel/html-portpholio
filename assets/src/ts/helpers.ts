export function debounce (callback: Function, time: number = 50) {
  let timeout = 0;
  return function () {
    const args = arguments;

    clearTimeout(timeout);
    timeout = setTimeout(() => {
      callback.apply(callback, args);
    }, time);
  }
};

export function trigger (target, event, params) {
    return target && target.dispatchEvent(new CustomEvent(event, params));
};