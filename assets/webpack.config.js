'use strict';
const isProd = process.argv.indexOf('-p') > -1;
const {resolve, join} = require('path');
const mode = isProd ? 'production' : 'development';
const webpack = require('webpack');

module.exports = {
  resolve: {
    extensions: ['.ts', '.js']
  },
  entry  : {
    main   : join(__dirname, 'src', 'ts', 'main.ts')
  },
  output : {
    path    : resolve(join(__dirname, 'dist', 'js')),
    filename: '[name].min.js',
  },
  mode   : mode,
  watch  : !isProd,
  devtool: isProd ? false : 'inline-source-map',
  module : {
    rules: [
    {
      test: /\.[jt]s$/,
      loader: 'ts-loader',
      exclude: /node_modules/
    }
    ]
  },
  plugins: [
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    'window.jQuery': 'jquery'
  }),
  ]
};